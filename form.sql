-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 07-07-2019 a las 15:05:43
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `form`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro`
--

CREATE TABLE `registro` (
  `id` int(11) NOT NULL,
  `Nombre` varchar(20) NOT NULL,
  `Apellido_paterno` varchar(20) NOT NULL,
  `Apellido_materno` varchar(20) NOT NULL,
  `Edad` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `registro`
--

INSERT INTO `registro` (`id`, `Nombre`, `Apellido_paterno`, `Apellido_materno`, `Edad`) VALUES
(2, 'Julissa', 'Delgado', 'guillermo', 0),
(5, 'Julissa', 'ddd', 'ss', 0),
(6, 'Julissa', 'ddd', 'ss', 0),
(7, 'Julissa', 'Delgado', 'guillermo', 0),
(8, 'Julissa', 'Delgado', 'yy', 0),
(9, 'Julissa', 'pe', 'dc', 99),
(10, 'perla', 'ruiz', 'dc', 99),
(11, 'Jose ', 'Delgado', 'Gut', 65),
(12, 'Guillermo', 'Delgado', 'e', 3),
(13, 'Julissa', 'Delgado', 'dc', 43),
(16, 'kakaaaaas', 'larios', 'wd', 12),
(17, 'Julissa', 'dde', 'da', 32),
(18, 'hhhhhhhhhhhhh', 'wwwwwww', 'fgrtg', 34),
(19, 'dssfsf', 'dfdfdfg', 'dfsd', 12),
(20, 'kuasasj', 'lsd,m lk', 'lsdkmk', 343),
(21, 'sdsdfdf', 'scd', '34', 5455),
(22, 'wdfdffffffffe', 'sdfffffffffff', 'freeeeeeeeeeee', 22),
(23, 'dssssssssssssssss', 'sfdsdffff', 'wffffffffffffff', 22),
(24, 'gggggggggggg', 'sssssssssssss', 'swwwwwwwwwwwwwww', 2),
(25, 'ffffffff', 'sssssssssss', 'wwwwwwwwwww', 22222222),
(26, 'sssssss', 'sssssssss', 'sssssss', 2112),
(27, 'qqqqqqq', 'qqqqqqqq', 'qqqq', 1212);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `registro`
--
ALTER TABLE `registro`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `registro`
--
ALTER TABLE `registro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
