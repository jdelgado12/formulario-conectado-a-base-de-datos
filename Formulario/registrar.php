<?php
require_once 'validaciones.php';
include 'cn.php';
$nombre = isset($_POST["nombre"])? $_POST['nombre']:null;
$paterno= isset($_POST["apPaterno"])? $_POST['apPaterno']:null;
$materno=isset($_POST["apMaterno"])? $_POST['apMaterno']:null;
$edad=isset($_POST["edad"])? $_POST['edad']:null;
$errores= array();


if ($_SERVER['REQUEST_METHOD']=='POST')
{
	if (!$nombre || !$paterno || !$materno || !$edad) 
		{ 
			echo "<font color='red'>COMPLETE LOS CAMPOS OBLIGATORIOS. GRACIAS.</font>";
		}
	if(!validaRe($nombre))
	{
		$errores[]='El campo nombre esta vacio';
	}
	if(!validaRe($paterno))
	{
		$errores[]='El campo Apellido paterno esta vacio';
	}
	if(!validaRe($materno))
	{
		$errores[]='El campo Apellido materno esta vacio';
	}
	if(!validaRe($edad))
	{
		$errores[]='El campo edad esta vacio';
	}

	$opciones_edad=array(
		'options'=>array(
			'min_range'=>1,
			'max_range'=>99));

	if(!validarEnt($edad,$opciones_edad))
	{
		$errores[]='El campo edad es incorrecto no cumple con las especificaciones.';
	}

	if(!soloLetras($nombre))
	{
		$errores[]='El campo nombre es incorrecto no cumple con las especificaciones.';
	}
	if(!soloLetras($paterno))
	{
		$errores[]='El campo Apellido paterno es incorrecto no cumple con las especificaciones.';
	}
	if(!soloLetras($materno))
	{
		$errores[]='El campo Apellido materno es incorrecto no cumple con las especificaciones.';
	}

	if(!$errores)
	{
		$nombreN=mysqli_real_escape_string($conexion,$nombre);
		$paternoN=mysqli_real_escape_string($conexion,$paterno);
		$maternoN=mysqli_real_escape_string($conexion,$materno);
		$edadN=mysqli_real_escape_string($conexion,$edad);
		$insertar = "INSERT INTO registro (Nombre,Apellido_paterno,Apellido_materno,Edad) VALUES ('$nombreN','$paternoN','$maternoN','$edadN')";

		$resultado = mysqli_query($conexion,$insertar); 
		?>
		<script type="text/javascript">
		alert("Registro guadado con exito");
		window.location.href='index.php';
		</script>

		<?php
		exit;
	}
	if($errores):?>
		<ul style="colo:#f00;">
		<?php foreach($errores as $error): ?>
			<li><?php echo $error ?></li>
		<?php endforeach; ?>
		<input type="submit" value="Regresar" id="regresa" name="regresa" onclick = "location='index.php'" >
	</ul>
<?php endif;

}



// if(!$resultado)
// {
// 	echo 'Error';
// }


mysqli_close($conexion);
//header("location: index.php")
?>



