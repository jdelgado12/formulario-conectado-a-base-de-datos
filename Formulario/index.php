<?php
include 'cn.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <link href="css/bootstrap.css" rel="stylesheet" type="text/css"> 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">


    <title>Formulario</title>

    <style>
    <style type="text/css">
    @import url("css/mycss.css");
         .header {
                background-color: #fff0;
                padding: 30px;
                color: #000;
                font-size: x-large;
                font-style: oblique;
            }

            #menu_principal {
                list-style: none;
                padding: 0;
                display: inline-block;
                margin: 0;
                float: right
            }
            #menu_principal li {
                float: left
            }
            #menu_principal a {
                color: inherit;
                padding: 0 15px;
                text-decoration: none
            }

             .home {
                height: 100%;
                position: relative;
                display: inline-block;
                width:100%;
                color: #fff
            }
            .bg-inicio {
                background-image: url('imagenes/im.jpg');
                background-position: center center;
                background-repeat: no-repeat;
                background-size: cover
            }
            .bg-inicio:before {
                display: block;
                content: '';
                width:100%;
                height: 100%;
                background-color: rgba(0,0,0,.5);
                position: absolute
            }
            .home .contain {
                position: relative;
                max-width: 980px;
                margin: 0 auto;
                display: flex;
                flex-flow: column;
                min-height: 100%


            }

            .home .contain::after,
            .home .contain::before {
             display: block;
             content: '';
             flex-grow: 1

            }
            .bg-normal{
                background-color: #fff3e0;
            }

              .flex  {
                  display: flex;

                   justify-content: center;

            }
            .flex > div {
              background-color: white;
              width: 100%;
              margin: 10px;
              text-align: center;
            }
            

              .cuad{
                color:  black;
                border: 1px solid white ;
                max-width: 1200px;
                margin: 0 auto;
                -webkit-box-shadow: 6px 11px 25px -10px rgba(0,0,0,0.58);
                -moz-box-shadow: 6px 11px 25px -10px rgba(0,0,0,0.58);
                box-shadow: 6px 11px 25px -10px rgba(0,0,0,0.58);
                border-radius: 0.5px;
                padding:5px;

                  }
            .mensaje{
                color:red;
            }
            #un_div { display : none; }

            #icono{
                padding: auto;
            }
    </style>
</head>
<body>

      <header class="header">
            <img src="imagenes/logo.png" width="85">

            <ul id="menu_principal">
                <li><a href="#">Inicio</a></li>
                <li><a href="#">Quienes somos</a></li>
                <li><a href="#">Proyectos</a></li>
                <li><a href="#">Contacto</a></li>
>
            </ul>
        </header  <section class="home bg-inicio">
          
        <div class="container">

        <section class="sec">
            <div class ="cuad">
            <div class="flex">
           <div >
        <h2>Registro</h2>
 
         
    <form action="registrar.php" method="post">
       <!--  <div class="mensaje">
        <label for="">Error campo requerido...</label>
        <output type="text" name="men" id="nom"> -->
    
        <div class="form-group">
            <label for="">Nombre</label>
            <input type="text" name="nombre" placeholder="EJ: Angel" idUser='520' value="" class="form-control" id="nombre" required pattern="[A-Za-zñÑ]{3,20}"
         title="Solo Letras. Tamaño mínimo: 3. Tamaño máximo: 20">
        </div>
       <div class="form-group">
            <label for="">Apelldio Paterno</label>
            <input type="text" name="apPaterno" placeholder="EJ: Salvador" idUser='520' value="" class="form-control" id="apPaterno" required pattern="[A-Za-z ñÑ]{3,20}" title="Solo Letras. Tamaño mínimo: 3. Tamaño máximo: 20">
        </div>
       <div class="form-group">
            <label for="">Apelldio Materno</label>
            <input type="text" name="apMaterno" placeholder="EJ: Castañeda" idUser='520' value="" class="form-control" id="apMaterno"required pattern="[A-Za-zñÑ]{3,20}"title="Solo Letras. Tamaño mínimo: 3. Tamaño máximo: 20">
        </div>
       <div class="form-group">
            <label for="">Edad</label>
            <input type="number" name="edad" placeholder="EJ: 21" idUser='520' value="" class="form-control" id="edad"required pattern="[0-9]{1,2}"
         title="Solo Letras. Tamaño mínimo: 1. Tamaño máximo: 2">
        </div>
          
        <div class="form-group">
            <input type="submit" value="Agregar" id="btnGuardar">
        </div>


    </form>
     <table class="table table-dark">

        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Apellido paterno</th> 
            <th>Apellido materno</th>
            <th>Edad</th>
        </tr>
        <?php
        $sql="SELECT * from registro";
        $result=mysqli_query($conexion,$sql);
        while($ver=mysqli_fetch_array($result))
        {
            ?>
        <tr>
        <td><?php echo $ver['id']?></td>
        <td><?php echo $ver['Nombre']?></td>
        <td><?php echo $ver['Apellido_paterno']?></td>
        <td><?php echo $ver['Apellido_materno']?></td>
        <td><?php echo $ver['Edad']?></td>
         <td> <a href='eliminar_prod.php?id=<?php echo $ver['id'];?>'><input type='submit' class='btn btn-danger' value="Eliminar"></a> </td>
         

    </tr>
    <?php
    }
    ?>
    </table>
    <section id="mostrar"></section>
    <script src="./form.js"></script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
     </div>
      </div>
    </section>
</body>
</html>


